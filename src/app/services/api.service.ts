import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  public url = 'http://patovega.com';
  public url2 = 'http://patovega.com';

  constructor( private httpClient: HttpClient) {
  }


  obtenerData() {
    let url = this.url + `/prueba_frontend/array.php`;
    return this.httpClient.get(url);
  }

  obtenerData2() {
    let url2 = this.url + `/prueba_frontend/dict.php`;
    return this.httpClient.get(url2);
  }


}
