import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-vista2',
  templateUrl: './vista2.component.html',
  styleUrls: ['./vista2.component.scss']
})
export class Vista2Component implements OnInit {

  public numero: number[];
  public paragraph: any[];
  constructor(public apiService: ApiService) { }

  ngOnInit() {
    // this.apiService.obtenerData2().subscribe((data: any) => {
    //   console.log(data);
    //   // this.paragraph = JSON.parse(data.data);
    // });

  }

  llenarTabla() {
    this.apiService.obtenerData2().subscribe((data: any) => {
      console.log(data);
      this.paragraph = JSON.parse(data.data);
    });
  }
}


