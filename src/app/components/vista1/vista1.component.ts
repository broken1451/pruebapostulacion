import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-vista1',
  templateUrl: './vista1.component.html',
  styleUrls: ['./vista1.component.scss']
})
export class Vista1Component implements OnInit {

  public numero: any[];
  public ocurrencia = { };
  public posicionArreglo: any;
  public posicionArregloUltimo: any;
  constructor(private apiservice: ApiService) {

  }

  ngOnInit() {

  }

  llenarTabla() {
    this.apiservice.obtenerData().subscribe((data: any) => {
      this.numero = data.data;

      for (var i = 0, j = this.numero.length; i < j; i++) {
       this.ocurrencia[this.numero[i]] = (this.ocurrencia[this.numero[i]] || 0) + 1;
       this.posicionArreglo = this.ocurrencia[this.numero[0]];
       this.posicionArregloUltimo = this.ocurrencia[this.numero.length - 1];
      //  console.log(this.ocurrencia);
      //  console.log('this.posicionArreglo: ', this.posicionArreglo);
      //  console.log('this.posicionArregloUltimo: ', this.posicionArregloUltimo);
      }
      // this.numero.sort();
      console.log(data);
      console.log(this.ocurrencia);
    });

  }

}


