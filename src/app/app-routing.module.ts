import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Vista1Component } from './components/vista1/vista1.component';
import { Vista2Component } from './components/vista2/vista2.component';



const routes: Routes = [

  { path: 'vista1', component: Vista1Component },
  { path: 'vista2', component: Vista2Component },
  { path: '**', component: Vista1Component },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

